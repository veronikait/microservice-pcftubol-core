package es.edu.cloudftic.pcfutbol.microservicepcftubolcore.services;

import es.edu.cloudftic.pcfutbol.microservicepcftubolcore.clients.TeamClient;
import es.edu.cloudftic.pcfutbol.microservicepcftubolcore.clients.TeamClientFeign;
import es.edu.cloudftic.pcfutbol.microservicepcftubolcore.components.NotificationHelper;
import es.edu.cloudftic.pcfutbol.microservicepcftubolcore.model.TeamDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Lazy
@Primary
@Service("ligaITA")
@ConditionalOnProperty(prefix = "info.app",name = "release",havingValue = "ARG")
public class TeamServiceArgImpl implements TeamService {

    @Autowired
    private TeamClientFeign teamClient;

    @Autowired
    private NotificationHelper notificationHelper;

    public Optional<TeamDTO> getTeamById(Integer id) {
        TeamDTO teamDTO = teamClient.getTeamById(id);
        return Optional.ofNullable(teamDTO);
    }

    public List<TeamDTO> getAllTeams(Pageable pageable) {

        List<TeamDTO> teamDTOS = List.of(new TeamDTO(1, "Milan"), new TeamDTO(2, "Inter"), new TeamDTO(3, "Sampdoria"));
        return teamDTOS;
    }

    public TeamDTO createTeam(TeamDTO teamDTO) {

        System.out.println("Creando equipo");
        notificationHelper.sendNotification("Nuevo equipo creado","Usted ha creado un equipo de manera satisfactoria");

        return teamDTO;
    }

    public TeamDTO updateTeam(Integer id,TeamDTO teamDTO) {

        System.out.println("Actualizando equipo");
        return teamDTO;
    }

    public Boolean deleteTeamById(Integer id) {

        System.out.println("Eliminando equipo");
        return true;
    }

}
