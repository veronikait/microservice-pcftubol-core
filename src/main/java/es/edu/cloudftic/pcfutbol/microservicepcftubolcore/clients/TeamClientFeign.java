package es.edu.cloudftic.pcfutbol.microservicepcftubolcore.clients;

import es.edu.cloudftic.pcfutbol.microservicepcftubolcore.model.TeamDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient( name = "market-consumer", url = "https://dddjsonplaceholder.typicode.com",fallback = TeamClientFeign.HystrixClientFallback.class)
public interface TeamClientFeign {

   @GetMapping(value = "/posts/{id}")
   TeamDTO getTeamById(@PathVariable Integer id);

   @Component
   static class HystrixClientFallback implements TeamClientFeign {

      @Override
      public TeamDTO getTeamById(Integer id) {

         //Plan B
         return new TeamDTO(1,"Empty Team");
      }
   }


}
