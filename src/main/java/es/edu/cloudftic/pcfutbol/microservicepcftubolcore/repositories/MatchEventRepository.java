package es.edu.cloudftic.pcfutbol.microservicepcftubolcore.repositories;

import es.edu.cloudftic.pcfutbol.microservicepcftubolcore.entities.MatchEventEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MatchEventRepository extends MongoRepository<MatchEventEntity,String> {


    public List<MatchEventEntity> findByEventLike(String event);

}

