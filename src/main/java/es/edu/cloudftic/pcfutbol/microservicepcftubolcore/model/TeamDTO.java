package es.edu.cloudftic.pcfutbol.microservicepcftubolcore.model;

import com.googlecode.jmapper.annotations.JMap;
import es.edu.cloudftic.pcfutbol.microservicepcftubolcore.validators.NIF;
import es.edu.cloudftic.pcfutbol.microservicepcftubolcore.validators.groups.OnCreate;
import es.edu.cloudftic.pcfutbol.microservicepcftubolcore.validators.groups.OnUpdate;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.persistence.Entity;
import javax.validation.constraints.*;
import java.time.LocalDate;
import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@ApiModel(description = "Equipo de futbol")
@Builder
public class TeamDTO {

    @ApiModelProperty(notes = "Unique identifier of the team.", example = "1", required = true, position = 0)
    @NonNull
    @JMap
    private Integer id;

    @ApiModelProperty(notes = "name of the team.", example = "River Plate", required = true, position = 1)
    @NonNull
    @NotBlank(message = "{team.field.name.validation.error}")
    @JMap
    private String name;

    @Min(1800)
    @Max(2000)
    @ApiModelProperty(notes = "foundation Year of the team.", example = "1903", required = true, position = 2)
    @JMap
    private Integer foundationYear;

    @Size(min = 4,max = 10)
    @ApiModelProperty(notes = "logo of the team.", example = "river.png", required = true, position = 3)
    private String logo;

    @AssertFalse(groups = OnUpdate.class)
    @AssertTrue(groups = OnCreate.class)
    @ApiModelProperty(notes = "Indicate if team is active or not",example = "true")
    private boolean active;

    @Email
    @ApiModelProperty(notes = "Indicate the team mail",example = "river@river.com")
    private String mail;


    //@Future
    //@FutureOrPresent
    //@PastOrPresent
    @Past
    @ApiModelProperty(notes = "Affiliate date team",example = "1903-01-01")
    private LocalDate affiliateFederationDate;


    //@Negative
    //@PositiveOrZero
    //@NegativeOrZero
    @Positive
    @ApiModelProperty(notes = "quantify of affiliates team",example = "10000")
    private int totalAffiliate;

    @NIF
    private String nif;

    private String title;

    private String body;

}
