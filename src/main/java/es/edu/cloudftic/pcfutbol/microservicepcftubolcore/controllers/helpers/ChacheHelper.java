package es.edu.cloudftic.pcfutbol.microservicepcftubolcore.controllers.helpers;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ChacheHelper {

    @Scheduled(fixedRate = 500000)
    @CacheEvict(value = "teams",allEntries = true)
    public void cleanTeamsCache(){

        System.out.println("limpiando cache de equipos");

    }


}
