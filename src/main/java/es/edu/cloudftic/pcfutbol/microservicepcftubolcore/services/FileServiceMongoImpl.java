package es.edu.cloudftic.pcfutbol.microservicepcftubolcore.services;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.client.gridfs.model.GridFSFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.FileUrlResource;
import org.springframework.core.io.Resource;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Paths;

@Primary
@Service
public class FileServiceMongoImpl implements FileService{

    @Autowired
    private GridFsTemplate gridFsTemplate;


   @Override
    public void store(MultipartFile file) throws IOException {

       DBObject metaData = new BasicDBObject();
       metaData.put("match", "match1");

       gridFsTemplate.store(file.getInputStream(), file.getOriginalFilename(), file.getContentType(),metaData);


   }

    @Override
    public Resource retrieveByFileName(String fileName) throws MalformedURLException {

        //GridFSFile one = gridFsTemplate.findOne(Query.query(Criteria.where("metadata.match").is("match1")));

        return gridFsTemplate.getResource(fileName);

    }
}
