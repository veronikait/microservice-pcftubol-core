package es.edu.cloudftic.pcfutbol.microservicepcftubolcore.clients;

import es.edu.cloudftic.pcfutbol.microservicepcftubolcore.model.TeamDTO;

public interface TeamClient {
	
	public TeamDTO getTeam(Integer id);

}

