package es.edu.cloudftic.pcfutbol.microservicepcftubolcore.components;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Component;

import java.util.concurrent.Future;

@EnableAsync
@Component
public class NotificationHelper {

   @Autowired
   private JavaMailSender emailSender;

   @Async
   public void sendNotification(String subject, String text) {

       SimpleMailMessage message = new SimpleMailMessage();
       message.setFrom("noreply@baeldung.com");
       message.setTo("mailpruebasdev@gmail.com");
       message.setSubject(subject);
       message.setText(text);

       emailSender.send(message);
   }


}
