package es.edu.cloudftic.pcfutbol.microservicepcftubolcore.entities;

import com.googlecode.jmapper.annotations.JMap;
import es.edu.cloudftic.pcfutbol.microservicepcftubolcore.validators.NIF;
import es.edu.cloudftic.pcfutbol.microservicepcftubolcore.validators.groups.OnCreate;
import es.edu.cloudftic.pcfutbol.microservicepcftubolcore.validators.groups.OnUpdate;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.LocalDate;

@Data
@NoArgsConstructor
@Entity(name = "teams")
public class TeamEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @JMap
    private Integer id;

    @JMap
    private String name;

    @JMap
    @Column(name ="foundation_year")
    private Integer foundationYear;

}
