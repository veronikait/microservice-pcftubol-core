package es.edu.cloudftic.pcfutbol.microservicepcftubolcore.services;

import com.googlecode.jmapper.JMapper;
import es.edu.cloudftic.pcfutbol.microservicepcftubolcore.exceptions.TeamNotFoundException;
import es.edu.cloudftic.pcfutbol.microservicepcftubolcore.entities.TeamEntity;
import es.edu.cloudftic.pcfutbol.microservicepcftubolcore.model.TeamDTO;
import es.edu.cloudftic.pcfutbol.microservicepcftubolcore.repositories.TeamRepository;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@RequiredArgsConstructor
@Service("ligaESP")
@ConditionalOnProperty(prefix = "info.app",name = "release",havingValue = "ESP")
public class TeamServiceImpl implements TeamService {

    @NonNull
    private TeamRepository teamRepository;


    public Optional<TeamDTO> getTeamById(Integer id)  {

        System.out.println("hola");

        Optional<TeamEntity> optTeamEntity = teamRepository.findById(id);
        TeamEntity teamEntity = optTeamEntity.orElseThrow(TeamNotFoundException::new);
        TeamDTO teamDTO = toTeamDTO(teamEntity);

        return Optional.ofNullable(teamDTO);
    }

    public List<TeamDTO> getAllTeams(Pageable pageable) {

        Page<TeamEntity> teamEntityPage = teamRepository.findAll(pageable);
        List<TeamEntity> teamEntities = teamEntityPage.getContent();

        /*List<TeamDTO> teamsDtos = new ArrayList<>();

        for (TeamEntity teamEntity : teamEntities) {

            TeamDTO teamDTO = toTeamDTO(teamEntity);
            teamsDtos.add(teamDTO);

        }*/

        List<TeamDTO> teamsDtos = teamEntities.
                                        stream().
                                        map(teamEntity -> toTeamDTO(teamEntity)).
                                        collect(Collectors.toList());


        return teamsDtos;
    }

    public TeamDTO createTeam(TeamDTO teamDTO) {

        TeamEntity teamEntity = toTeamEntity(teamDTO);
        teamEntity = teamRepository.save(teamEntity);
        teamDTO = toTeamDTO(teamEntity);

        System.out.println("Creando equipo " + teamDTO.getId());
        return teamDTO;
    }

    public TeamDTO updateTeam(Integer id,TeamDTO teamDTO) {

        teamDTO.setId(id);
        Optional<TeamEntity> optTeamEntity = teamRepository.findById(id);
        TeamEntity teamEntity = optTeamEntity.get();

        TeamEntity teamEntityToUpdate = toTeamEntity(teamDTO);
        teamRepository.save(teamEntityToUpdate);

        System.out.println("Actualizando equipo");
        return teamDTO;
    }

    public Boolean deleteTeamById(Integer id) {

        Optional<TeamEntity> optTeamEntity = teamRepository.findById(id);
        teamRepository.deleteById(id);
        System.out.println("Eliminando equipo");
        return true;
    }


    public TeamDTO toTeamDTO(TeamEntity teamEntity){

        JMapper<TeamDTO, TeamEntity> userMapper = new JMapper<>(TeamDTO.class, TeamEntity.class);
        TeamDTO teamDTO = userMapper.getDestination(teamEntity);
        return teamDTO;

        /*return TeamDTO.builder().
                    id(teamEntity.getId()).
                    name(teamEntity.getName()).
                    foundationYear(teamEntity.getFoundationYear()).build();*/

        /*TeamDTO teamDTO = new TeamDTO();
        teamDTO.setId(teamEntity.getId());
        teamDTO.setName(teamEntity.getName());
        teamDTO.setFoundationYear(teamEntity.getFoundationYear());
        return teamDTO;*/
    }


    public TeamEntity toTeamEntity(TeamDTO teamDTO){

        JMapper<TeamEntity,TeamDTO> userMapper = new JMapper<>(TeamEntity.class,TeamDTO.class);
        TeamEntity teamEntity = userMapper.getDestination(teamDTO);
        return teamEntity;

        /*
        TeamEntity teamEntity = new TeamEntity();
        teamEntity.setId(teamDTO.getId());
        teamEntity.setName(teamDTO.getName());
        teamEntity.setFoundationYear(teamDTO.getFoundationYear());
        return teamEntity;*/
    }

}
