package es.edu.cloudftic.pcfutbol.microservicepcftubolcore.controllers;

import com.sun.mail.iap.Response;
import es.edu.cloudftic.pcfutbol.microservicepcftubolcore.services.FileService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.MalformedURLException;

@RequiredArgsConstructor
@RestController
@RequestMapping("files")
public class FileControllerRest {


    @NonNull
    private FileService fileService;


    @PostMapping
    public ResponseEntity<?> uploadFile(@RequestParam MultipartFile[] files) throws IOException {

        for (MultipartFile file: files) {

            System.out.println("FIle: " + file.getOriginalFilename());
            fileService.store(file);

        }


        return ResponseEntity.ok().build();
    }

    @GetMapping("/{fileName}")
    public ResponseEntity<?> serveFile(@PathVariable String fileName) throws MalformedURLException {

        System.out.println("Solicitando el fichero " + fileName);

        Resource resource = fileService.retrieveByFileName(fileName);

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename()+ "\"")
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .body(resource);

    }


}
