package es.edu.cloudftic.pcfutbol.microservicepcftubolcore.controllers;

import es.edu.cloudftic.pcfutbol.microservicepcftubolcore.entities.MatchEventEntity;
import es.edu.cloudftic.pcfutbol.microservicepcftubolcore.repositories.MatchEventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Random;

@RestController
public class MatchController {

    @Autowired
    MatchEventRepository matchEventRepository;

    @GetMapping("matches/play")
    public ResponseEntity<?> playMatch(){

        Random rnd = new Random();
        List<String> events = List.of("Saque de meta", "Corner", "Pase al fondo", "Pase", "Gooooooool", "Penallll!!!", "Tiro libre", "Saque de banda");


        for (int i = 0; i < 90; i++) {

            System.out.println("minuto " + i + " de juego");
            String event = events.get(rnd.nextInt(events.size() - 1));

            MatchEventEntity matchEventEntity = new MatchEventEntity(event);
            matchEventRepository.save(matchEventEntity);

        }

        return ResponseEntity.ok().build();
    }

    @GetMapping("matches")
    public ResponseEntity<?> findAllEvents(){

        List<MatchEventEntity> events = matchEventRepository.findAll();

        return ResponseEntity.ok(events);

    }

    @GetMapping("matches/{event}")
    public ResponseEntity<?> findAllEvents(@PathVariable String event){

        List<MatchEventEntity> events = matchEventRepository.findByEventLike(event);

        return ResponseEntity.ok(events);

    }



}
