package es.edu.cloudftic.pcfutbol.microservicepcftubolcore.repositories;

import es.edu.cloudftic.pcfutbol.microservicepcftubolcore.entities.TeamEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TeamRepository extends JpaRepository<TeamEntity, Integer> {

    List<TeamEntity> findByFoundationYearLessThan(int foundationYear);

    List<TeamEntity> findByFoundationYearGreaterThanEqual(int foundationYear);

    List<TeamEntity> findByNameLike(String name);

    List<TeamEntity> findByNameContaining(String name);

    @Query(value = "select * from teams where name = ?1 and foundation_year >= ?2 and foundation_year <= ?3", nativeQuery = true)
    List<TeamEntity> findAllTeamsBetweenYearAndName(String name, int yearStart, int yearEnd);


}
