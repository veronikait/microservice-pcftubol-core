package es.edu.cloudftic.pcfutbol.microservicepcftubolcore.entities;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

//Colleccion de mongo equivalente a una tabla de base de datos
@RequiredArgsConstructor
@Data
@Document
public class MatchEventEntity {

    @MongoId
    private String id;

    @NonNull
    private String event;

}
