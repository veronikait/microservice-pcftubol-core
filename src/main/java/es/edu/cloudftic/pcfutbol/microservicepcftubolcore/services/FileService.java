package es.edu.cloudftic.pcfutbol.microservicepcftubolcore.services;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.MalformedURLException;

public interface FileService {

    public void store(MultipartFile file) throws IOException;

    public Resource retrieveByFileName(String fileName) throws MalformedURLException;
}
