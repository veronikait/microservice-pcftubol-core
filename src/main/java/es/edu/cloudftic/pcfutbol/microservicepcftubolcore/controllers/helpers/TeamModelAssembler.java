package es.edu.cloudftic.pcfutbol.microservicepcftubolcore.controllers.helpers;

import es.edu.cloudftic.pcfutbol.microservicepcftubolcore.controllers.TeamControllerRest;
import es.edu.cloudftic.pcfutbol.microservicepcftubolcore.model.TeamDTO;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class TeamModelAssembler implements RepresentationModelAssembler<TeamDTO, EntityModel<TeamDTO>> {


    @Override
    public EntityModel<TeamDTO> toModel(TeamDTO teamDTO) {

        return EntityModel.of(teamDTO,
                linkTo(methodOn(TeamControllerRest.class).getTeamById(teamDTO.getId())).withSelfRel(),
                linkTo(methodOn(TeamControllerRest.class).getAllTeams(null)).withRel("teams"),
                linkTo(methodOn(TeamControllerRest.class).getAllPlayerByTeamId(teamDTO.getId())).withRel("players"));

        //1. Link es una relacion self (consigo misma)
        //2. Link para recuperar todos los teams
    }

    @Override
    public CollectionModel<EntityModel<TeamDTO>> toCollectionModel(Iterable<? extends TeamDTO> teams) {

        List<TeamDTO> teamsList = (List<TeamDTO>) teams;

        List<EntityModel<TeamDTO>> teamEntitiesModel =
                teamsList.stream().map(team -> toModel(team)).collect(Collectors.toList());

        CollectionModel<EntityModel<TeamDTO>> teamsCollectionModel = CollectionModel.of(
                    teamEntitiesModel, linkTo(methodOn(TeamControllerRest.class).getAllTeams(null)).withSelfRel());

        return teamsCollectionModel;
    }
}
