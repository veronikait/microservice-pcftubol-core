package es.edu.cloudftic.pcfutbol.microservicepcftubolcore.controllers;


import es.edu.cloudftic.pcfutbol.microservicepcftubolcore.controllers.helpers.TeamModelAssembler;
import es.edu.cloudftic.pcfutbol.microservicepcftubolcore.model.PlayerDTO;
import es.edu.cloudftic.pcfutbol.microservicepcftubolcore.model.TeamDTO;
import es.edu.cloudftic.pcfutbol.microservicepcftubolcore.services.TeamService;
import es.edu.cloudftic.pcfutbol.microservicepcftubolcore.services.TeamServiceImpl;
import es.edu.cloudftic.pcfutbol.microservicepcftubolcore.validators.groups.OnCreate;
import es.edu.cloudftic.pcfutbol.microservicepcftubolcore.validators.groups.OnUpdate;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@RequestMapping("/teams")
@RestController
@Api(tags = "API de equipos de futbol")
public class TeamControllerRest {

    //@Autowired
    private TeamModelAssembler teamModelAssembler;

    @Autowired
    private PagedResourcesAssembler<TeamDTO> pagedResourcesAssembler;


    //@Qualifier("ligaESP")
    //@Autowired
    private TeamService teamService;

    public TeamControllerRest(@Lazy TeamService teamService, TeamModelAssembler teamModelAssembler){

        this.teamService = teamService;
        this.teamModelAssembler = teamModelAssembler;
        System.out.println("iniciando team controller rest");
    }

   /* @ApiResponses(value = {
            @ApiResponse(code = 400,message = "Bad Request")
    })*/
    @ApiOperation(notes="Retrieve one team by id",value="Get team by id")
    @GetMapping("/{id}")
    public ResponseEntity<?> getTeamById(
            @ApiParam(example = "1", value = "Identifier for Team", allowableValues = "1,2,3,4", required = true)
            @PathVariable Integer id) {

        Optional<TeamDTO> optTeamDto = teamService.getTeamById(id);

        EntityModel<TeamDTO> entityModel = optTeamDto.
                                            map(teamDTO -> teamModelAssembler.toModel(teamDTO)).
                                            get();


        return ResponseEntity.ok(entityModel);

    }

    @Cacheable("teams")
    @GetMapping
    public ResponseEntity<?> getAllTeams(
                    //@PageableDefault(size = 3,sort = {"id","name"},direction = Sort.Direction.ASC)
                    Pageable pageable) {


        System.out.println("ejecutando metodo getAllTeams()");

        List<TeamDTO> teamDTOS = teamService.getAllTeams(pageable);

        Page<TeamDTO> pageTeam = new PageImpl<TeamDTO>(teamDTOS, pageable, teamDTOS.size());

        PagedModel<EntityModel<TeamDTO>> pagedModelTeamsDtos = pagedResourcesAssembler.toModel(pageTeam, teamModelAssembler);

        //CollectionModel<EntityModel<TeamDTO>> teamsDtoEntitiesModel = teamModelAssembler.toCollectionModel(teamDTOS);

        /**
         * Algrotimo costoso
         */

        //Java 8
        // List<TeamDTO> list = new ArrayList<>();
        // list.add(new TeamDTO(1,"Real Madrid") );
        return ResponseEntity.ok(pagedModelTeamsDtos);

    }

    @CacheEvict(value = "teams",allEntries = true)
    @PostMapping
    public ResponseEntity<?> createTeam(@Validated(OnCreate.class) @RequestBody TeamDTO teamDTO) {

        System.out.println("creando equipo");

        teamDTO = teamService.createTeam(teamDTO);

        EntityModel<TeamDTO> teamDTOEntityModel = teamModelAssembler.toModel(teamDTO);


        /*URI location = ServletUriComponentsBuilder.
                fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(teamDTO.getId())
                .toUri();*/

        return ResponseEntity.
                created(teamDTOEntityModel.getRequiredLink(IanaLinkRelations.SELF).toUri())
                .body(teamDTOEntityModel);
    }


    @PutMapping("/{id}")
    public ResponseEntity<?> updateTeam(@PathVariable Integer id,@Validated(OnUpdate.class) @RequestBody TeamDTO teamDTO) {

        teamDTO = teamService.updateTeam(id,teamDTO);

        EntityModel<TeamDTO> teamDTOEntityModel = teamModelAssembler.toModel(teamDTO);

        return ResponseEntity.ok(teamDTOEntityModel);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteById(@PathVariable Integer id) {

        teamService.deleteTeamById(id);

        System.out.println("Eliminando un recurso");

        return ResponseEntity.noContent().build();
    }

    @GetMapping("/{id}/players")
    public ResponseEntity<List<PlayerDTO>> getAllPlayerByTeamId(@PathVariable Integer id) {

        List<PlayerDTO> players = new ArrayList<>();
        players.add(new PlayerDTO("Ronaldo"));
        players.add(new PlayerDTO("Butrageño"));
        players.add(new PlayerDTO("Valderrama"));

        return ResponseEntity.ok(players);

    }


    @GetMapping("/{id}/players/{idPlayer}")
    public ResponseEntity<PlayerDTO> getOnePlayerByTeamId(@PathVariable Integer id, @PathVariable Integer idPlayer) {

        return ResponseEntity.ok(new PlayerDTO("Valderrama"));

    }


}
