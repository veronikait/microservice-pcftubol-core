package es.edu.cloudftic.pcfutbol.microservicepcftubolcore.model;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;


@Data
@RequiredArgsConstructor
public class PlayerDTO {

    @NonNull
    private String name;

    private Integer number;

    private String position;

}
