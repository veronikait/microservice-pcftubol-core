package es.edu.cloudftic.pcfutbol.microservicepcftubolcore.validators;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = NIFValidator.class)
@Target( { ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface NIF {
   String message() default "Invalid NIF Number";
   Class<?>[] groups() default {};
   Class<? extends Payload>[] payload() default {};
}

