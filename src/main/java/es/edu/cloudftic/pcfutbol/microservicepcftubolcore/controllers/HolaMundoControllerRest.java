package es.edu.cloudftic.pcfutbol.microservicepcftubolcore.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import es.edu.cloudftic.pcfutbol.microservicepcftubolcore.configurations.AppInfo;
import es.edu.cloudftic.pcfutbol.microservicepcftubolcore.model.TeamDTO;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.mobile.device.Device;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

@RestController
@Data
public class HolaMundoControllerRest {

    @Autowired
    private AppInfo appInfo;

    @Autowired
    private ObjectMapper objectMapper;

    @GetMapping("/holaMundo/{user}")
    public String holaMundo(@PathVariable String user, Device device) throws IOException {

        //Data file
        Resource data = appInfo.getData();
        System.out.println(data.getFilename());
        List<String> lines = Files.readAllLines(data.getFile().toPath());
        lines.forEach(line -> System.out.println(line));

        //Json File
        Resource json = appInfo.getEquipo();
        //Deserealizando json a objeto java
        TeamDTO teamDTO = objectMapper.readValue(json.getFile(), TeamDTO.class);

        String platform = "";

        if (device.isNormal()){
            platform = "desktop";
        }else if (device.isMobile()){
            platform = "mobile";
        }else if (device.isTablet()){
            platform = "tablet";
        }


        return "Hola Mundo Rest ("+platform+") " + user + " - " + appInfo + " - equipo  " + teamDTO;
    }

}
