package es.edu.cloudftic.pcfutbol.microservicepcftubolcore.configurations;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.Resource;

import java.util.Arrays;
import java.util.Map;

@Data
@Configuration
@ConfigurationProperties(prefix = "info.app")
@PropertySource(value = "classpath:torneos.properties")
public class AppInfo {

    private String name;

    //@Value("${year}")
    private Integer year;

    //@Value("${app.edition}")
    private String edition;

    //@Value("${app.countries}")
    private String[] countries;

    @Value("${JAVA_HOME}")
    private String javaHomeVariableDeEntorno;

    @Value("Rafael Benedettelli")
    private String author;

    private String[] torneos;

    @Value("classpath:data.txt")
    private Resource data;

    @Value("classpath:equipo.json")
    private Resource equipo;


}
