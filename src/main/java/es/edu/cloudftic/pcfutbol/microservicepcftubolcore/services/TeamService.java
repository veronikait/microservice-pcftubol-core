package es.edu.cloudftic.pcfutbol.microservicepcftubolcore.services;

import es.edu.cloudftic.pcfutbol.microservicepcftubolcore.model.TeamDTO;
import org.springframework.data.domain.Pageable;

import java.rmi.NoSuchObjectException;
import java.util.List;
import java.util.Optional;

public interface TeamService {

    public Optional<TeamDTO> getTeamById(Integer id);

    public List<TeamDTO> getAllTeams(Pageable pageable);

    public TeamDTO createTeam(TeamDTO teamDTO);

    public TeamDTO updateTeam(Integer id,TeamDTO teamDTO);

    public Boolean deleteTeamById(Integer id);

}
