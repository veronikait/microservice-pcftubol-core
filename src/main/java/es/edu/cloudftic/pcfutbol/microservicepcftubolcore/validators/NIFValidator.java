package es.edu.cloudftic.pcfutbol.microservicepcftubolcore.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class NIFValidator implements ConstraintValidator<NIF, String>{

  @Override
  public boolean isValid(String value, ConstraintValidatorContext context) {

     if (value == null) {
        return false;
     }

     return value.length()==9;

  }
 
}
