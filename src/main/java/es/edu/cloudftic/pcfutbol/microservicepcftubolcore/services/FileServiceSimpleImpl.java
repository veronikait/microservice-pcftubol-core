package es.edu.cloudftic.pcfutbol.microservicepcftubolcore.services;

import org.springframework.context.annotation.Primary;
import org.springframework.core.io.FileUrlResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Paths;

@Service
public class FileServiceSimpleImpl implements FileService{

    private final static String DEFAULT_PATH = "c:\\data\\prueba\\";

   @Override
    public void store(MultipartFile file) throws IOException {

       file.transferTo(Paths.get(DEFAULT_PATH + file.getOriginalFilename()));

    }

    @Override
    public Resource retrieveByFileName(String fileName) throws MalformedURLException {

        return new FileUrlResource(DEFAULT_PATH + fileName  );
    }
}
