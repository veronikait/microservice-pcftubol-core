package es.edu.cloudftic.pcfutbol.microservicepcftubolcore.controllers.errors;

import es.edu.cloudftic.pcfutbol.microservicepcftubolcore.exceptions.TeamNotFoundException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MaxUploadSizeExceededException;

import java.io.FileNotFoundException;

@ControllerAdvice
public class GlobalExceptionHandle {

    @Value("${spring.servlet.multipart.max-file-size}")
    private String maxSizeLimit;

    @ResponseBody
    @ExceptionHandler(TeamNotFoundException.class)
    public ApiError handleTeamNotFound(){

        return new ApiError(HttpStatus.NOT_FOUND,"No se encuentra el equipo",null);

    }


    @ResponseBody
    @ExceptionHandler(MaxUploadSizeExceededException.class)
    public ApiError handleSizeLimit(){

        return new ApiError(HttpStatus.EXPECTATION_FAILED,"Se ha superado el tamaño maximo de " + maxSizeLimit,null);

    }


    @ResponseBody
    @ExceptionHandler(FileNotFoundException.class)
    public ApiError handleFileNotFound(){

        return new ApiError(HttpStatus.NOT_FOUND,"no se encontro el archivo",null);

    }

}
